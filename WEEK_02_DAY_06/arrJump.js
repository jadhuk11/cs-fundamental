// [2, 5, 4, 2, 1, 10, 8, 3, 4]

// arr, 0, 2
// 2 + 4 + 1 + 8 + 4 = 19

// arr. 4, 3
// 1 + 3 = 4

let arr =[2, 5, 4, 2, 1, 10, 8, 3, 4] //9

function answer(arr, start, jump) {
 let result = 0;
 for(let i = start; i < arr.length; i += jump){
     result += arr[i];
 }
 return result
}

answer(arr, 0, 2);