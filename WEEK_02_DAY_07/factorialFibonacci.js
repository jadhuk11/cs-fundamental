function factorial(n) {
    if(n==0) { // 2 == 0
        return 1; // jika n = 0 pake sebelah kiri yang ini
    }
    // kalo ngga pake yang dibawah ini
    return n * factorial(n-1); // 5 * 4 * 3 * 2 * 1 * 1;
}

function fibonacci(n) {
    if(n==0) {
        return 0;
    } else if(n==1) {
        return 1;
    }
    return fibonacci(n-1) + fibonacci(n-2);
}

 console.log(fibonacci(6));

// fibonaci(5) + fibonaci(4)

// fibonaci(4) + fibonaci(3) + fibonaci(3) + fibonaci(2)

// fibonaci(3) + fibonaci(2) + fibonaci(2) + fibonaci(1) + fibonaci(3) + fibonaci(2)

// fibonaci(1) + fibonaci(1) +  fibonaci(1) +  fibonaci(1) + fibonaci(1)+ fibonaci(1) + fibonaci(1) + fibonaci(1)


// 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1
// 8;


// 5! => 5 * 4 * 3 * 2 * 1