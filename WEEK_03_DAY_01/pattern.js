let n = 10;

// for(i=0; i<n; i++) {  
//     let data = "";
//     for(j=0; j<n; j++ ) { 
//         data += "*"
//     }
//     console.log(data);
// }

// output :
// *****
// *****
// *****
// *****
// *****

for(i=0; i<n; i++) {  
    let data = "";
    for(j=0; j<n; j++) { 
        if(i==0 || i==n-1 || j==0 || j==n-1){
            data += "*"
        }   
        else{
            data += " ";
        }
    console.log(data);
}
}

// output :
// *****
// *   *
// *   *
// *   *
// *****



// for (i= 0; i< n; i++){
//     let data = "";
//     for(j=0; j<n-i; j++){
//         data += "*";
//     }
//     console.log(data)
// }

// output :
// *****
// ****
// ***
// **
// *

// for (i= 0; i< n; i++){
//     let data = "";
//     for(j=0; j<i+1; j++){
//         data += "*";
//     }
//     console.log(data)
// }

// output :
// *
// **
// ***
// ****
// *****
