
// 3849
// MMMDCCCXLIX

// M => 1000 => 1000
// M => 1000 => 2000
// M => 1000 => 3000
// D => 500 => 3500
// C => 100 => 3600
// C => 100 => 3700
// C => 100 => 3800
// X => -10 => 3790
// L => 50 => 3840
// I => -1 => 3839
// X => 10 => 3849

function convertRomanToNumber(roman){
    
    const romanMapping =["I","V","X","L","C","D","M"];
    const valueMapping =[1,5,10,50,100,500,1000];
    
    let result = 0;
    for (i = 0; i < roman.length; i++){
        const currentRomanIndex = romanMapping.indexOf(roman[i])
        const nextRomanIndex = romanMapping.indexOf(roman[i+1])

        const currentIndexValue = valueMapping[currentRomanIndex]
        const nextIndexValue = valueMapping[nextRomanIndex]
        if (currentIndexValue < nextIndexValue){
            result -= currentIndexValue
        }else{
            result += currentIndexValue
        }
    }
        return result;

}

// value = valueMapping[i]
// valueNext = valueMapping[i+1]
// jika value >= valueMapping[i]
// maka (x-value)

console.log(convertRomanToNumber("CCMXI"));

