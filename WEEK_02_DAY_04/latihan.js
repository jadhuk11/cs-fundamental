const {regencies,provinces} = require('./location');



function getCities(provinceName) {
    const cities = []

    let provinceId = 0;

    // find province where province name equal provinceName
    for(let province of provinces) {
        if(province.name == provinceName) {
            provinceId = province.id;
            break;
        }
    }

    // find city where province_id equals provinceId
    for(let regency of regencies) {
        if(regency.province_id == provinceId) {
            cities.push(regency.name);
        }
    }

    return cities;
}


const nameProvince = "RIAU";
const cities = getCities(nameProvince);
console.log(cities)