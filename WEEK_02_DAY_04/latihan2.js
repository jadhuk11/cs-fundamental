const {regencies,provinces} = require('./location');


// find province where city name equal regencies name

function getProvinces(regenciesName) {
    const prov = []

    let regenciesId = 0;

    // find province where province name equal provinceName
    for(let regency of regencies) {
        if(regency.name == regenciesName) {
            regenciesId = regency.province_id;
            break;
        }
    }

    // find city where province_id equals provinceId
    for(let province of provinces) {
        if(province.id == regenciesId) {
            prov.push(province.name);
        }
    }

    return prov;
}


const nameRegencies = "KOTA JAMBI";
const prov = getProvinces(nameRegencies);
console.log(prov)